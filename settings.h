#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QString>
#include <QFont>

class Settings : public QObject
{
    Q_OBJECT
public:
    Settings(QObject *parent = nullptr);

    QString getCurrentStyleName() const;
    void setCurrentStyleName(const QString &value);

    QFont getCurrentFont() const;
    void setCurrentFont(const QFont &value);

    int getCurrentTabSize() const;
    void setCurrentTabSize(int value);

    bool getPortableMode() const;
    void setPortableMode(bool value);

signals:
    void settingsChanged();

private:
    QString currentStyleName;
    QFont currentFont;
    int currentTabSize;
    bool portableMode;
};

#endif // SETTINGS_H
