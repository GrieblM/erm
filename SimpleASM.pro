#-------------------------------------------------
#
# Project created by QtCreator 2017-01-25T21:03:47
#
#-------------------------------------------------

QT       += core gui svg concurrent widgets

TARGET = SimpleASM
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    linenumberarea.cpp \
    codeeditor.cpp \
    asmhighlighter.cpp \
    tokenizer.cpp \
    registerdelegate.cpp \
    register.cpp \
    virtualmashine.cpp \
    registeritemmodel.cpp \
#    settingsdialog.cpp \
    helpdialog.cpp \
    aboutdialog.cpp \
#    settings.cpp
    messageboxstyle.cpp \
    licensedialog.cpp

HEADERS  += mainwindow.h \
    linenumberarea.h \
    codeeditor.h \
    asmhighlighter.h \
    tokenizer.h \
    registerdelegate.h \
    register.h \
    vmtypes.h \
    virtualmashine.h \
    registeritemmodel.h \
#    settingsdialog.h \
    helpdialog.h \
    aboutdialog.h \
#    settings.h
    messageboxstyle.h \
    licensedialog.h

FORMS    += mainwindow.ui \
#    settingsdialog.ui \
    helpdialog.ui \
    aboutdialog.ui \
    licensedialog.ui

RESOURCES += \
    resources.qrc

TRANSLATIONS += strings_de.ts

win32:RC_ICONS = SimpleASM.ico
