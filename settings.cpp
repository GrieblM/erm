#include "settings.h"

extern QString defaultStyleName;

Settings::Settings(QObject *parent) : QObject(parent)
{
}

QString Settings::getCurrentStyleName() const
{
    return currentStyleName;
}

void Settings::setCurrentStyleName(const QString &value)
{
    currentStyleName = value;
    emit settingsChanged();
}

QFont Settings::getCurrentFont() const
{
    return currentFont;
}

void Settings::setCurrentFont(const QFont &value)
{
    currentFont = value;
    emit settingsChanged();
}

int Settings::getCurrentTabSize() const
{
    return currentTabSize;
}

void Settings::setCurrentTabSize(int value)
{
    currentTabSize = value;
    emit settingsChanged();
}

bool Settings::getPortableMode() const
{
    return portableMode;
}

void Settings::setPortableMode(bool value)
{
    portableMode = value;
    emit settingsChanged();
}
