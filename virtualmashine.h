#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <QObject>
#include <QList>
#include <QFuture>
#include <QFutureWatcher>

#include "vmtypes.h"
#include "register.h"

class VirtualMashine : public QObject
{
    Q_OBJECT
public:
    VirtualMashine(QObject *parent = nullptr);
    ~VirtualMashine();

    Register *getRegister();
    QFutureWatcher<void> *getRunWathcer();

public slots:
    void runAsync();
    void run();
    void cancel();
    void singleStep();
    void reset();
    void reinstruct(QList<Instruction> instructions);

signals:
    void executionFinished();
    void stepped(int line);
    void error(QString msg, int line);

private:
    void doStep();
    Instruction getNextInstruction(bool *ok);

    QFutureWatcher<void> *runWatcher;

    QList<Instruction> instructions;
    Register reg;
    bool hasEnded = false;
    int stepCount = 0;
};

#endif // INTERPRETER_H
