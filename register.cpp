#include "register.h"
#include <QtGlobal>

Register::Register()
{
    reset();
}

int Register::counter()
{
    return _counter;
}

void Register::setCounter(int value)
{
    _counter = value;
    emit registerChanged(0);
}

void Register::increaseCounter()
{
    _counter++;
    emit registerChanged(0);
}

int Register::getAccumulator()
{
    return _accumulator;
}

void Register::setAccumulator(int value)
{
    _accumulator = value;
    emit registerChanged(1);
}

int &Register::operator[](int index)
{
    Q_ASSERT(index >= 0 && index <= 15);
    return _register[index];
}

void Register::setRegister(int index, int value)
{
    Q_ASSERT(index >= 0 && index <= 15);
    _register[index] = value;
    emit registerChanged(index + 2);
}

void Register::add(int value)
{
    _accumulator += value;
    emit registerChanged(1);
}

void Register::sub(int value)
{
    _accumulator -= value;
    emit registerChanged(1);
}

void Register::mul(int value)
{
    _accumulator *= value;
    emit registerChanged(1);
}

void Register::div(int value)
{
    _accumulator  /= value;
    emit registerChanged(1);
}

void Register::reset()
{
    memset(&_register, 0, sizeof(_register));
    _accumulator = 0;
    _counter = 1;
    for(int i = 0; i <= 17; i++)
        emit registerChanged(i);
}
