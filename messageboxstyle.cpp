#include "messageboxstyle.h"

MessageBoxStyle::MessageBoxStyle()
{
}

QIcon MessageBoxStyle::standardIcon(QStyle::StandardPixmap standardIcon, const QStyleOption *option, const QWidget *widget) const
{
    QIcon icon;
    switch (standardIcon)
    {
    case SP_DialogSaveButton:
        icon.addFile(":/document-save.svg", QSize(22, 22));
        return icon;
    case SP_DialogDiscardButton:
        icon.addFile(":/edit-clear.svg", QSize(22, 22));
        return icon;
    case SP_DialogCancelButton:
    case SP_DialogCloseButton:
        icon.addFile(":/dialog-cancel.svg", QSize(22, 22));
        return icon;
    case SP_DialogOkButton:
        icon.addFile(":/dialog-ok-apply.svg", QSize(22, 22));
        return icon;
    case SP_MessageBoxCritical:
        icon.addFile(":/dialog-error.svg", QSize(64, 64));
        return icon;
    case SP_MessageBoxInformation:
        icon.addFile(":/dialog-information.svg", QSize(64, 64));
        return icon;
    case SP_MessageBoxQuestion:
        icon.addFile(":/dialog-question.svg", QSize(64, 64));
        return icon;
    case SP_MessageBoxWarning:
        icon.addFile(":/dialog-warning.svg", QSize(64, 64));
        return icon;
    default:
        return QProxyStyle::standardIcon(standardIcon, option, widget);
    }
}
