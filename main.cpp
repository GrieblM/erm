#include "messageboxstyle.h"
#include "mainwindow.h"

#include <QApplication>
#include <QTranslator>
#include <QFile>
#include <QDataStream>
#include <QStyle>
#include <QStandardPaths>
#include <QScopedPointer>
#include <QLibraryInfo>
#include <QFontDatabase>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setStyle(new MessageBoxStyle);

    QFontDatabase::addApplicationFont(":/NotoMono-Regular.ttf");

    QTranslator translator;
    translator.load(QLocale(), "strings", "_", ":/translations/");
    a.installTranslator(&translator);

    QTranslator qtTranslator;
    qtTranslator.load(QLocale(), "qt", "_", QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    a.installTranslator(&qtTranslator);

    MainWindow w;
    w.show();

    return a.exec();
}
