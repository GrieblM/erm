#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QTimer>

#include "virtualmashine.h"
#include "registeritemmodel.h"
#include "tokenizer.h"

class AsmHighlighter;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *e) override;

private slots:
    void on_actionNew_triggered();
    void on_actionOpen_triggered();
    void on_actionSave_triggered();
    void on_actionSaveAs_triggered();
#if false
    void on_actionSettings_triggered();
#endif
    void on_actionExit_triggered();

    void on_actionUndo_triggered();
    void on_actionRedo_triggered();
    void on_actionCut_triggered();
    void on_actionCopy_triggered();
    void on_actionPaste_triggered();

    void on_actionStatusBar_triggered(bool checked);
    void on_actionRegisters_triggered(bool checked);

    void on_actionRun_triggered();
    void on_actionStep_triggered();
    void on_actionReset_triggered();

    void on_actionAbout_triggered();
    void on_actionHelp_triggered();

    void on_stepButton_clicked();
    void on_resetButton_clicked();

    void runtimeError(QString msg, int line);

    void onInput();
    void reset();
    void stepped(int line);
#if false
    void settingsChanged();
#endif

    void runFinished();
    void runCanceled();

    void resetStatusBar();
private:
    Ui::MainWindow *ui;
    QTimer *statusBarTimer;

    AsmHighlighter *highlighter;
    VirtualMashine *vm;
    Tokenizer *tk;
    RegisterItemModel *model;
    QLabel *statusBarLabel;

    bool fileNamed = false;
    //true, if vm vm is loaded with the current code
    bool loaded = false;

    QString fileName = tr("Untitled");
    QString path;

    void load();
    void fileChanged();
    bool showUnsavedChangesDialog();
    bool saveFile();
    void openFile(QString filePath);
};

#endif // MAINWINDOW_H
