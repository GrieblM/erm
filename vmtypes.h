#ifndef VMTYPES_H
#define VMTYPES_H

enum Operator
{
    LOAD,
    DLOAD,
    STORE,
    ADD,
    SUB,
    MUL,
    DIV,
    JUMP,
    JEQ,
    JNE,
    JGT,
    JGE,
    JLT,
    JLE,
    END
};

struct Instruction
{
    Operator op;
    int arg;
    int line;
};

#endif // VMTYPES_H
