#ifndef REGISTER_H
#define REGISTER_H

#include <QObject>

class Register : public QObject
{
    Q_OBJECT
public:
    Register();

    int counter();
    void setCounter(int value);
    void increaseCounter();

    int getAccumulator();
    void setAccumulator(int value);

    int& operator[](int index);
    void setRegister(int index, int value);

    void add(int value);
    void sub(int value);
    void mul(int value);
    void div(int value);

    void reset();
signals:
    void registerChanged(int index);
private:
    int _counter = 0;
    int _accumulator = 0;
    int _register[16];
};

#endif // REGISTER_H
