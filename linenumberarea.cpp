#include "linenumberarea.h"

LineNumberArea::LineNumberArea(CodeEditor *parent) : QWidget(parent)
{
    editor = parent;
}

QSize LineNumberArea::sizeHint() const
{
    return QSize(editor->lineNumberAreaWidth(), 0);
}

void LineNumberArea::paintEvent(QPaintEvent *event)
{
    editor->lineNumberAreaPaintEvent(event);
}
