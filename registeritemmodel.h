#ifndef REGISTERITEMMODEL_H
#define REGISTERITEMMODEL_H

#include <QAbstractTableModel>

#include "register.h"

class RegisterItemModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    RegisterItemModel(Register *r, QObject *parent = nullptr);

    Qt::ItemFlags flags(const QModelIndex&) const override;

    int rowCount(const QModelIndex&) const override;
    int columnCount(const QModelIndex&) const override;

    QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
private slots:
    void fireDataChanged(int index);
private:
    Register *reg;
};

#endif // REGISTERITEMMODEL_H
