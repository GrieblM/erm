#include "settings.h"
#include "settingsdialog.h"
#include "ui_settingsdialog.h"

extern Settings settings;
extern QString defaultStyleName;

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
#ifdef Q_OS_LINUX
    ui->styleComboBox->addItem(QIcon(), tr("GTK")); //GTK-Style is only available on linux
#endif

    ui->fontComboBox->setFont(settings.getCurrentFont());
    ui->fontSizeSpinner->setValue(settings.getCurrentFont().pointSize());
    ui->tabSizeSpinner->setValue(settings.getCurrentTabSize());
    ui->portableModeCheckBox->setChecked(settings.getPortableMode());
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::on_styleComboBox_currentIndexChanged(int index)
{
    switch (index) {
    case 0:
        currentStyle = defaultStyleName;
        break;
    case 1:
        currentStyle = "Fusion";
        break;
#ifdef Q_OS_LINUX
    case 2:
        currentStyle = "GTK";
        break;
#endif
    }
}

void SettingsDialog::accept()
{
    QDialog::accept();
    QFont font = ui->fontComboBox->font();
    font.setPointSize(ui->fontSizeSpinner->value());
    settings.setCurrentStyleName(currentStyle);
    settings.setCurrentFont(font);
    settings.setCurrentTabSize(ui->tabSizeSpinner->value());
    settings.setPortableMode(ui->portableModeCheckBox->isChecked());
}
