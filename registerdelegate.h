#ifndef REGISTERDELEGATE_H
#define REGISTERDELEGATE_H

#include <QItemDelegate>

class RegisterDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    RegisterDelegate(QObject *parent);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem&, const QModelIndex& index) const override;
    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
};

#endif // REGISTERDELEGATE_H
