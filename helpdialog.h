#ifndef HELPDIALOG_H
#define HELPDIALOG_H

#include <QDialog>
#include <QTemporaryFile>

namespace Ui {
class HelpDialog;
}

class HelpDialog : public QDialog
{
    Q_OBJECT

public:
    explicit HelpDialog(QWidget *parent = 0);
    ~HelpDialog();

private slots:
    void on_browserButton_clicked();
    void on_closeButton_clicked();

private:
    Ui::HelpDialog *ui;
    QTemporaryFile *tempfile;

    bool fileCreated = false;
    bool createFile();
};

#endif // HELPDIALOG_H
