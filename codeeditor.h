#ifndef CODEEDITOR_H
#define CODEEDITOR_H

#include <QPlainTextEdit>
#include <QCompleter>

class CodeEditor : public QPlainTextEdit
{
    Q_OBJECT
public:
    CodeEditor(QWidget *parent = nullptr);
    ~CodeEditor();

    int lineNumberAreaWidth();
    void lineNumberAreaPaintEvent(QPaintEvent *e);
public slots:
    void highlightLine(int line, bool error = false);
protected:
    void resizeEvent(QResizeEvent *e) override;
    void focusInEvent(QFocusEvent *e) override;
    void keyPressEvent(QKeyEvent *e) override;
    void dropEvent(QDropEvent *e) override;
private:
    QWidget *lineNumberArea;
    QCompleter *completer;
private slots:
    void updateLineNumberAreaWidth(int);
    void updateLineNumberArea(const QRect &rect, int dy);
    void unhighlightLine();

    void completeWord(QString word);
};

#endif // CODEEDITOR_H
