#include "aboutdialog.h"
#include "ui_aboutdialog.h"
#include "licensedialog.h"

#include <QUrl>
#include <QFile>
#include <QTextBrowser>
#include <QDesktopServices>
#include <QMessageBox>

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent, Qt::WindowTitleHint | Qt::WindowCloseButtonHint | Qt::WindowSystemMenuHint),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);

    connect(ui->infoLabel, SIGNAL(linkActivated(QString)), this, SLOT(linkClicked(QString)));
    connect(ui->licensesLabel, SIGNAL(linkActivated(QString)), this, SLOT(linkClicked(QString)));
}

AboutDialog::~AboutDialog()
{
    delete ui;
}

void AboutDialog::linkClicked(QString link)
{
    //Our internal resource links start with :
    if(link.startsWith(':'))
        LicenseDialog(link, this).exec();
    else
        QDesktopServices::openUrl(QUrl(link));
}
