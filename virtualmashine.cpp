#include "virtualmashine.h"
#include <QtConcurrent>

VirtualMashine::VirtualMashine(QObject *parent) : QObject(parent), runWatcher(new QFutureWatcher<void>(this))
{
}

VirtualMashine::~VirtualMashine()
{
    delete runWatcher;
}

void VirtualMashine::runAsync()
{
    QFuture<void> future = QtConcurrent::run(this, &VirtualMashine::run);
    runWatcher->setFuture(future);
}

void VirtualMashine::run()
{
    reg.setCounter(1);
    stepCount = 0;
    hasEnded = false;
    while(!hasEnded && reg.counter() <= instructions.length())
    {
        if(stepCount >= 10000)
        {
            emit error(tr("Execution seems to be stuck. Aborting..."), 0);
            break;
        }
        doStep();
        stepCount++;
    }
}

void VirtualMashine::cancel()
{
    runWatcher->cancel();
}

void VirtualMashine::singleStep()
{
    if(instructions.length() == 0)
        return;
    doStep();
    if(hasEnded || reg.counter() > instructions.length())
    {
        reg.setCounter(1);
        hasEnded = false;
    }
    emit stepped(instructions[reg.counter() - 1].line);
}

void VirtualMashine::reset()
{
    reg.reset();
}

void VirtualMashine::reinstruct(QList<Instruction> inst)
{
    instructions.clear();
    instructions << inst;
}

Instruction VirtualMashine::getNextInstruction(bool *ok)
{
    if(instructions.isEmpty() || reg.counter() > instructions.length())
    {
        (*ok) = false;
        return Instruction();
    }
    (*ok) = true;
    //We need to decrement the counter by one, because our counter starts at one
    return instructions[reg.counter() - 1];
}

Register *VirtualMashine::getRegister()
{
    return &reg;
}

QFutureWatcher<void> *VirtualMashine::getRunWathcer()
{
    return runWatcher;
}


void VirtualMashine::doStep()
{
    bool ok;
    Instruction nextStep = getNextInstruction(&ok);

    if(!ok)
    {
        hasEnded = true;
        return;
    }

    switch (nextStep.op)
    {
    case LOAD:
        if(nextStep.arg < 0 || nextStep.arg > 15)
        {
            emit error(tr("Can't load from specified register. Only registers 0 to 15 are allowed."), nextStep.line);
            hasEnded = true;
            return;
        }
        reg.setAccumulator(reg[nextStep.arg]);
        break;
    case DLOAD:
        reg.setAccumulator(nextStep.arg);
        break;
    case STORE:
        if(nextStep.arg < 0 || nextStep.arg > 15)
        {
            emit error(tr("Only registers 0 to 15 are allowed."), nextStep.line);
            hasEnded = true;
            return;
        }
        reg.setRegister(nextStep.arg, reg.getAccumulator());
        break;
    case ADD:
        reg.add(reg[nextStep.arg]);
        break;
    case SUB:
        reg.sub(reg[nextStep.arg]);
        break;
    case MUL:
        reg.mul(reg[nextStep.arg]);
        break;
    case DIV:
        if(reg[nextStep.arg] == 0)
        {
            emit error(tr("Cannot devide by zero."), nextStep.line);
            hasEnded = true;
            return;
        }
        reg.div(reg[nextStep.arg]);
        break;
    case JUMP:
        reg.setCounter(nextStep.arg);
        return;
    case JEQ:
        if(reg.getAccumulator() == 0)
        {
            reg.setCounter(nextStep.arg);
            return;
        }
        break;
    case JNE:
        if(reg.getAccumulator() != 0)
        {
            reg.setCounter(nextStep.arg);
            return;
        }
        break;
    case JGT:
        if(reg.getAccumulator() > 0)
        {
            reg.setCounter(nextStep.arg);
            return;
        }
        break;
    case JGE:
        if(reg.getAccumulator() >= 0)
        {
            reg.setCounter(nextStep.arg);
            return;
        }
        break;
    case JLT:
        if(reg.getAccumulator() < 0)
        {
            reg.setCounter(nextStep.arg);
            return;
        }
        break;
    case JLE:
        if(reg.getAccumulator() <= 0)
        {
            reg.setCounter(nextStep.arg);
            return;
        }
        break;
    case END:
        hasEnded = true;
        return;
    }
    reg.increaseCounter();
}
