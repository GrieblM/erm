#include "tokenizer.h"
#include <QRegularExpression>
#include <QDebug>

Tokenizer::Tokenizer(QObject *parent) : QObject(parent)
{
    keywords["LOAD"] = LOAD;
    keywords["DLOAD"] = DLOAD;
    keywords["STORE"] = STORE;
    keywords["ADD"] = ADD;
    keywords["SUB"] = SUB;
    keywords["MUL"] = MUL;
    keywords["MULT"] = MUL;
    keywords["DIV"] = DIV;
    keywords["JUMP"] = JUMP;
    keywords["JEQ"] = JEQ;
    keywords["JNE"] = JNE;
    keywords["JLT"] = JLT;
    keywords["JLE"] = JLE;
    keywords["JGT"] = JGT;
    keywords["JGE"] = JGE;
    keywords["END"] = END;
}

//Used to break out of loops using a goto
void dummy(){}

QList<Instruction> Tokenizer::tokenize(QString code, bool *ok)
{
    (*ok) = true;
    QList<Instruction> instructions;
    QStringList lines = code.split('\n');
    for (int i = 0; i < lines.length(); i++)
    {
        QString line = lines[i];
        if(line.isEmpty())
            //Ignore empty lines
            continue;
        //The instruction to be written to the stream
        Instruction inst;
        inst.line = i + 1;
        QStringList words = line.split(QRegExp("[ \t]"));
        bool operatorSet = false;
        bool operandSet = false;
        foreach (QString word, words)
        {
            bool convertSuccessfull = false;
            int operand;
            //Ignore the rest of the line, if a word is or starts with --
            if(QRegularExpression("(--[^\n]*|#[^\n]*|;[^\n]*)").match(word).hasMatch())
                goto end;
            //Set the operator, if it is a keyword
            else if(keywords.contains(word.toUpper()))
            {
                inst.op = keywords[word.toUpper()];
                operatorSet = true;
            }
            else
            {
                //We need some extra code to check if word is an integer
                operand = word.toInt(&convertSuccessfull);
                if(convertSuccessfull)
                {
                    inst.arg = operand;
                    operandSet = true;
                }
                else
                {
                    emit error(tr("Syntax error in line %0").arg(i+1), i+1);
                    (*ok) = false;
                    return QList<Instruction>();
                }
            }
        }
        //End of line procession...
        end:
        //Check, if the instruction is valid and should be added
        if(operatorSet && (operandSet || inst.op == END))
            instructions.append(inst);
    }
    return instructions;
}
