#include "asmhighlighter.h"

AsmHighlighter::AsmHighlighter(QTextDocument *parent) : QSyntaxHighlighter(parent)
{
    HighlightingRule rule;

    keywordFormat.setForeground(Qt::blue);
    commentFormat.setForeground(Qt::darkGreen);

    QStringList keywords;
    keywords << "\\bLOAD\\b" << "\\bDLOAD\\b" << "\\bSTORE\\b" << "\\bADD\\b"
                  << "\\bSUB\\b" << "\\bMUL\\b" << "\\bMULT\\b" << "\\bDIV\\b" << "\\bJUMP\\b"
                  << "\\bJEQ\\b"   << "\\bJNE\\b" << "\\bJLT\\b" << "\\bJLE\\b"
                  << "\\bJGT\\b" << "\\bJGE\\b" << "\\bEND\\b";

    foreach (const QString &word, keywords)
    {
        rule.pattern = QRegExp(word, Qt::CaseInsensitive);
        rule.format = keywordFormat;
        rules.append(rule);
    }

    //Single line comment
    rule.format = commentFormat;
    rule.pattern = QRegExp("(--[^\n]*|#[^\n]*|;[^\n]*)");
    rules.append(rule);
}

void AsmHighlighter::highlightBlock(const QString &text)
{
    foreach (const HighlightingRule &rule, rules)
    {
        QRegExp exp(rule.pattern);
        int index = exp.indexIn(text);
        while(index > -1)
        {
            int length = exp.matchedLength();
            setFormat(index, length, rule.format);
            index = exp.indexIn(text, index + length);
        }
    }
}
