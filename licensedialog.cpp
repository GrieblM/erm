#include "licensedialog.h"
#include "ui_licensedialog.h"

#include <QFile>
#include <QMessageBox>

LicenseDialog::LicenseDialog(QString path, QWidget *parent) :
    QDialog(parent, Qt::WindowTitleHint | Qt::WindowCloseButtonHint | Qt::WindowSystemMenuHint),
    ui(new Ui::LicenseDialog)
{
    ui->setupUi(this);

    QFile file(path);
    if(file.open(QIODevice::ReadOnly))
        ui->licenseText->setText(file.readAll());
    else
        QMessageBox::critical(this, tr("Error"), tr("File could not be opened.\n%0").arg(file.errorString()));
}

LicenseDialog::~LicenseDialog()
{
    delete ui;
}
