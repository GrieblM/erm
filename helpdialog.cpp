#include "helpdialog.h"
#include "ui_helpdialog.h"

#include <QFile>
#include <QDir>
#include <QDesktopServices>
#include <QTextStream>
#include <QMessageBox>

HelpDialog::HelpDialog(QWidget *parent) :
    QDialog(parent, Qt::WindowTitleHint | Qt::WindowMinimizeButtonHint |
            Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint |
            Qt::WindowSystemMenuHint),
    ui(new Ui::HelpDialog),
    tempfile(new QTemporaryFile(QString("%0/XXXXXX_help.html").arg(QDir::tempPath())))
{
    ui->setupUi(this);
    QFile f(tr(":/html/help_en.html"));
    f.open(QIODevice::ReadOnly);
    ui->textBrowser->setHtml(f.readAll());
    f.close();
}

HelpDialog::~HelpDialog()
{
    delete ui;
    delete tempfile;
}

void HelpDialog::on_browserButton_clicked()
{
    if(createFile())
        QDesktopServices::openUrl(tempfile->fileName());
}

void HelpDialog::on_closeButton_clicked()
{
    close();
}

bool HelpDialog::createFile()
{
    if(fileCreated)
        return true;
    if(tempfile->open())
    {
        QFile html(tr(":/html/help_en.html"));
        html.open(QIODevice::ReadOnly);
        QTextStream out(tempfile);
        out << html.readAll();
        tempfile->close();
        return true;
    }
    QMessageBox::critical(this, tr("Error"), tr("File could not be opened.\n%0").arg(tempfile->errorString()));
    return false;
}
