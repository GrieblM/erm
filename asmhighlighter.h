#ifndef ASMHIGHLIGHTER_H
#define ASMHIGHLIGHTER_H

#include <QSyntaxHighlighter>

class AsmHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT
public:
    AsmHighlighter(QTextDocument *parent = nullptr);

protected:
    void highlightBlock(const QString &text) override;
private:
    struct HighlightingRule
    {
        QRegExp pattern;
        QTextCharFormat format;
    };

    QVector<HighlightingRule> rules;

    QTextCharFormat keywordFormat;
    QTextCharFormat commentFormat;
};

#endif // ASMHIGHLIGHTER_H
