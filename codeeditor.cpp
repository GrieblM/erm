#include "codeeditor.h"
#include "linenumberarea.h"

#include <QPainter>
#include <QTextBlock>
#include <QAbstractItemView>

#include <cmath>

CodeEditor::CodeEditor(QWidget *parent) : QPlainTextEdit(parent)
{
    lineNumberArea = new LineNumberArea(this);
    connect(this, SIGNAL(blockCountChanged(int)), this, SLOT(updateLineNumberAreaWidth(int)));
    connect(this, SIGNAL(updateRequest(QRect,int)), this, SLOT(updateLineNumberArea(QRect,int)));

    completer = new QCompleter(this);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setCompletionMode(QCompleter::PopupCompletion);
    connect(completer, SIGNAL(activated(QString)), this, SLOT(completeWord(QString)));

    setFont(QFont("Noto Mono", 11)); //TODO: Set platform console font instead of Windows one
    updateLineNumberAreaWidth(0);
}

CodeEditor::~CodeEditor()
{
    disconnect(this, SIGNAL(textChanged()), this, SLOT(unhighlightLine()));
}

int CodeEditor::lineNumberAreaWidth()
{
    int digits = 1;
    int lines = std::max(digits, document()->lineCount());
    while (lines >= 10)
    {
        lines /= 10;
        ++digits;
    }

    return 6 + fontMetrics().width('9') * digits;
}

void CodeEditor::lineNumberAreaPaintEvent(QPaintEvent *e)
{
    QPainter painter(lineNumberArea);
    painter.setPen(QPen(QBrush(Qt::darkGray), 0.5));
    painter.drawLine(e->rect().topRight(), e->rect().bottomRight());
    painter.setPen(Qt::darkGray);

    QTextBlock block = firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
    int bottom = top + (int) blockBoundingRect(block).height();

    while (block.isValid() && top <= e->rect().bottom())
    {
        if (block.isVisible() && bottom >= e->rect().top())
        {
            QString number = QString::number(blockNumber + 1);
            painter.drawText(0, top, lineNumberArea->width() - 3, fontMetrics().height(),Qt::AlignRight, number);
        }

        block = block.next();
        top = bottom;
        bottom = top + (int) blockBoundingRect(block).height();
        ++blockNumber;
    }
}

void CodeEditor::highlightLine(int line, bool error)
{
    unhighlightLine();
    if(line > 0)
    {
        QTextBlock block = document()->findBlockByLineNumber(line - 1);
        int pos = block.position();
        QTextCursor cur = textCursor();
        cur.setPosition(pos);
        cur.movePosition(QTextCursor::EndOfLine);

        QList<QTextEdit::ExtraSelection> selections;
        QTextEdit::ExtraSelection selection;
        selection.cursor = cur;
        selection.format.setBackground(error ? Qt::red : Qt::yellow);
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selections.append(selection);
        setExtraSelections(selections);

        connect(this, SIGNAL(textChanged()), this, SLOT(unhighlightLine()));
    }
}

void CodeEditor::resizeEvent(QResizeEvent *e)
{
    QPlainTextEdit::resizeEvent(e);

    QRect cr = contentsRect();
    lineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.bottom()));
}

void CodeEditor::focusInEvent(QFocusEvent *e)
{
    QPlainTextEdit::focusInEvent(e);
    if(completer)
        completer->setWidget(this);
}

void CodeEditor::keyPressEvent(QKeyEvent *e)
{
    if(completer->popup()->isVisible())
    {
        //We don't want to handle these events as the Completer shall do that
        switch (e->key())
        {
        case Qt::Key_Enter:
        case Qt::Key_Return:
        case Qt::Key_Escape:
        case Qt::Key_Tab:
        case Qt::Key_Backtab:
            e->ignore();
            return;
        default:
            break;
        }
    }

    QPlainTextEdit::keyPressEvent(e);
}

void CodeEditor::dropEvent(QDropEvent *e)
{
    QPlainTextEdit::dropEvent(e);
}

void CodeEditor::updateLineNumberAreaWidth(int)
{
    setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
}

void CodeEditor::updateLineNumberArea(const QRect &rect, int dy)
{
    if (dy)
        lineNumberArea->scroll(0, dy);
    else
        lineNumberArea->update(0, rect.y(), lineNumberArea->width(), rect.height());

    if (rect.contains(viewport()->rect()))
        updateLineNumberAreaWidth(0);
}

void CodeEditor::unhighlightLine()
{
    setExtraSelections(QList<QTextEdit::ExtraSelection>());
    disconnect(this, SIGNAL(textChanged()), this, SLOT(unhighlightLine()));
}

void CodeEditor::completeWord(QString word)
{
    QTextCursor cur = textCursor();
    cur.beginEditBlock();
    cur.movePosition(QTextCursor::StartOfWord);
    cur.movePosition(QTextCursor::EndOfWord, QTextCursor::KeepAnchor);
    cur.insertText(word);
    cur.endEditBlock();
}
