<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="aboutdialog.ui" line="14"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="66"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;SimpleASM&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;SimpleASM&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="79"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Version 1.1&lt;/p&gt;&lt;p&gt;SimpleASM is an editor/emulator for a simple, assembly-like language.&lt;br/&gt;Licenced under the &lt;a href=&quot;:/licenses/gpl-2.0.txt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;GNU General Public Licence v2.0&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Repository: &lt;a href=&quot;https://bitbucket.org/GrieblM/simpleasm&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://bitbucket.org/GrieblM/simpleasm&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Version 1.0&lt;/p&gt;&lt;p&gt;SimpleASM is a editor/emulator for a simple, assembly-like language.&lt;br/&gt;Licenced under the GNU General Public Licence v2.0&lt;/p&gt;&lt;p&gt;Repository: &lt;a href=&quot;https://bitbucket.org/GrieblM/simpleasm&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://bitbucket.org/GrieblM/simpleasm&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Version 1.1&lt;/p&gt;&lt;p&gt;SimpleASM ist ein Editor für eine einfache, assembly-artige Sprache.&lt;br/&gt;Lizenziert unter der &lt;a href=&quot;:/licenses/gpl-2.0.txt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;GNU General Public Licence v2.0&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;Repository: &lt;a href=&quot;https://bitbucket.org/GrieblM/simpleasm&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://bitbucket.org/GrieblM/simpleasm&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="89"/>
        <source>Software used</source>
        <translation>Verwendete Software</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="95"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Qt (&lt;a href=&quot;:/licenses/third-party-licenses/lgpl-3.0.txt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;LGPL&lt;/span&gt;&lt;/a&gt;)&lt;/p&gt;&lt;p&gt;Breeze icons (&lt;a href=&quot;:/licenses/third-party-licenses/lgpl-2.1.txt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;LGPL&lt;/span&gt;&lt;/a&gt;)&lt;/p&gt;&lt;p&gt;Noto Mono font (&lt;a href=&quot;:/licenses/third-party-licenses/ofl.txt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;OFL&lt;/span&gt;&lt;/a&gt;)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Qt (&lt;a href=&quot;:/third-party-licenses/lgpl-3.0.txt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;LGPL&lt;/span&gt;&lt;/a&gt;)&lt;/p&gt;&lt;p&gt;Breeze icons (LGPL)&lt;/p&gt;&lt;p&gt;Noto Mono font (OFL)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation></translation>
    </message>
</context>
<context>
    <name>HelpDialog</name>
    <message>
        <location filename="helpdialog.ui" line="14"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="helpdialog.ui" line="32"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Help&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="helpdialog.ui" line="70"/>
        <source>Open in Browser</source>
        <translation>Im Browser öffnen</translation>
    </message>
    <message>
        <location filename="helpdialog.ui" line="80"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="helpdialog.cpp" line="18"/>
        <location filename="helpdialog.cpp" line="47"/>
        <source>:/html/help_en.html</source>
        <translation>:/html/help_de.html</translation>
    </message>
    <message>
        <location filename="helpdialog.cpp" line="54"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="helpdialog.cpp" line="54"/>
        <source>File could not be opened.
%0</source>
        <translation>Die Datei konnte nicht geöffnet werden.
%0</translation>
    </message>
</context>
<context>
    <name>LicenseDialog</name>
    <message>
        <location filename="licensedialog.ui" line="14"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location filename="licensedialog.cpp" line="17"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="licensedialog.cpp" line="17"/>
        <source>File could not be opened.
%0</source>
        <translation>Die Datei konnte nicht geöffnet werden.
%0</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Untitled - SimpleASM</source>
        <translation>Unbenannt - SimpleASM</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="74"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="85"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="96"/>
        <source>&amp;View</source>
        <translation>A&amp;nsicht</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="103"/>
        <location filename="mainwindow.ui" line="354"/>
        <source>&amp;Run</source>
        <translation>&amp;Ausführen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="111"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="144"/>
        <source>Registers</source>
        <translation>Register</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="209"/>
        <source>Step</source>
        <translation>Schritt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="226"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="252"/>
        <source>&amp;New</source>
        <translation>&amp;Neu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="255"/>
        <source>Creates a new file</source>
        <translation>Erstellt eine neue Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="258"/>
        <source>Ctrl+N</source>
        <translation>Strg+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="267"/>
        <source>&amp;Open</source>
        <translation>&amp;Öffnen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="270"/>
        <source>Open a file</source>
        <translation>Öffnet eine Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="273"/>
        <source>Ctrl+O</source>
        <translation>Strg+O</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="282"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="285"/>
        <source>Save the current file</source>
        <translation>Speichert die aktuelle Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="288"/>
        <source>Ctrl+S</source>
        <translation>Strg+S</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="297"/>
        <source>Save &amp;as</source>
        <translation>Speichern &amp;unter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="300"/>
        <source>Save the current file under a new name</source>
        <translation>Speichert die aktuelle Datei unter einem neuen Namen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="303"/>
        <source>Ctrl+Shift+S</source>
        <translation>Strg+Umschalt+S</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="312"/>
        <source>E&amp;xit</source>
        <translation>S&amp;chließen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="315"/>
        <source>Quits the application</source>
        <translation>Beendet das Programm</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="324"/>
        <source>&amp;Undo</source>
        <translation>&amp;Rückgängig</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="327"/>
        <source>Undos the last action</source>
        <translation>Macht die letzte Aktion rückgängig</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="330"/>
        <source>Ctrl+Z</source>
        <translation>Strg+Z</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="339"/>
        <source>&amp;Redo</source>
        <translation>&amp;Wiederholen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="342"/>
        <source>Redo the just undone action</source>
        <translation>Die rückgängig gemachte Aktion wiederholen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="345"/>
        <source>Ctrl+Y</source>
        <translation>Strg+Y</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="357"/>
        <source>Runs the current program</source>
        <translation>Führt das Programm aus</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="360"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="369"/>
        <source>&amp;Step</source>
        <translation>&amp;Schritt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="372"/>
        <source>Runs the next step of the program</source>
        <translation>Führt den nächsten Schritt des Programms aus</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="381"/>
        <source>Re&amp;set</source>
        <translation>&amp;Zurücksetzen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="384"/>
        <source>Resets the current program and the register</source>
        <translation>Setzt das Register auf den Standardwert zurück</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="393"/>
        <source>C&amp;ut</source>
        <translation>&amp;Ausschneiden</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="396"/>
        <source>Cuts out the selected text</source>
        <translation>Schneidet den ausgewählten aus</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="399"/>
        <source>Ctrl+X</source>
        <translation>Strg+X</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="408"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopieren</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="411"/>
        <source>Copies the selected text to the clipbord</source>
        <translation>Kopiert den ausgewählten Text in die Zwischenabage</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="414"/>
        <source>Ctrl+C</source>
        <translation>Strg+C</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="423"/>
        <source>&amp;Paste</source>
        <translation>&amp;Einfügen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="426"/>
        <source>Pastes the content of the clipbord at the cursor position</source>
        <translation>Fügt den Inhalt der Zwischenablage am Cursor ein</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="429"/>
        <source>Ctrl+V</source>
        <translation>Strg+V</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="438"/>
        <source>&amp;About</source>
        <translation>&amp;Über</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="441"/>
        <source>Displays information about this program</source>
        <translation>Zeigt Informationen zum Programm an</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="450"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="453"/>
        <source>Displays help about this Program and the sasm syntax</source>
        <translation>Zeigt Hilfe zum SASM-Syntax an</translation>
    </message>
    <message>
        <source>F&amp;ind</source>
        <translation type="vanished">&amp;Suchen</translation>
    </message>
    <message>
        <source>searches for a given string in the editor</source>
        <translation type="vanished">Durchsucht das Dokument nach einer gegebenen Zeichenfolge</translation>
    </message>
    <message>
        <source>Ctrl+F</source>
        <translation type="vanished">Strg+F</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="464"/>
        <source>&amp;Status bar</source>
        <translation>&amp;Statusleiste</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="475"/>
        <source>&amp;Registers</source>
        <translation>&amp;Register</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Einstellungen</translation>
    </message>
    <message>
        <source>&amp;Abort execution</source>
        <translation type="vanished">Ausführung &amp;abbrechen</translation>
    </message>
    <message>
        <source>Abort execution</source>
        <translation type="vanished">Ausführung abbrechen</translation>
    </message>
    <message>
        <source>Stops executing the current script</source>
        <translation type="vanished">Beendet die Ausführung des Skriptes</translation>
    </message>
    <message>
        <source>Ctrl+Del</source>
        <translation type="vanished">Strg+Entf</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="43"/>
        <location filename="mainwindow.cpp" line="279"/>
        <source>Ready</source>
        <translation>Bereit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="84"/>
        <location filename="mainwindow.h" line="83"/>
        <source>Untitled</source>
        <translation>Unbenannt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="91"/>
        <source>Open assembly file</source>
        <translation>Assembly-Datei öffnen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="91"/>
        <location filename="mainwindow.cpp" line="129"/>
        <source>Simple Assembly file (*.sasm);;Assembly file (*.asm);;All files (*)</source>
        <translation>Simple-Assembly-Datei (*.sasm);;Assembly-Datei(*.asm);;Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="97"/>
        <source>The file could not be opened</source>
        <translation>Die Datei konnte nicht geöffnet werden</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="97"/>
        <location filename="mainwindow.cpp" line="118"/>
        <location filename="mainwindow.cpp" line="135"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="118"/>
        <location filename="mainwindow.cpp" line="135"/>
        <source>The file could not be opened.
%0</source>
        <translation>Die Datei konnte nicht geöffnet werden
%0</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="129"/>
        <source>Save file</source>
        <translation>Datei speichern</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="296"/>
        <source>%0 - SimpleASM</source>
        <translation>%0 - SimpleASM</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="307"/>
        <source>Unsaved changes exist in this file. Do you wan&apos;t to save it?</source>
        <translation>Die Datei wurde geändert. Wollen sie diese Änderungen speichern?</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Speichern</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="vanished">Verwerfen</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Abbrechen</translation>
    </message>
</context>
<context>
    <name>RegisterItemModel</name>
    <message>
        <location filename="registeritemmodel.cpp" line="45"/>
        <source>Counter</source>
        <translation>Zähler</translation>
    </message>
    <message>
        <location filename="registeritemmodel.cpp" line="47"/>
        <source>Accumulator</source>
        <translation>Akkumulator</translation>
    </message>
    <message>
        <location filename="registeritemmodel.cpp" line="49"/>
        <source>Register %0</source>
        <translation>Register %0</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <source>Settings</source>
        <translation type="vanished">Einstellungen</translation>
    </message>
    <message>
        <source>Style:</source>
        <translation type="vanished">Style:</translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="vanished">Standard</translation>
    </message>
    <message>
        <source>Fusion</source>
        <translation type="vanished">Fusion</translation>
    </message>
    <message>
        <source>Font:</source>
        <translation type="vanished">Schriftart:</translation>
    </message>
    <message>
        <source>Tab size:</source>
        <translation type="vanished">Tabgröße:</translation>
    </message>
    <message>
        <source>Portable mode</source>
        <translation type="vanished">Einstellungen im gleichen Verzeichnis speichern</translation>
    </message>
    <message>
        <source>GTK</source>
        <translation type="vanished">GTK</translation>
    </message>
</context>
<context>
    <name>Tokenizer</name>
    <message>
        <location filename="tokenizer.cpp" line="69"/>
        <source>Syntax error in line %0</source>
        <translation>Syntax-Error in Zeile %0</translation>
    </message>
</context>
<context>
    <name>VirtualMashine</name>
    <message>
        <source>Can&apos;t load from specified register. Only registers 1 to 15 are allowed.</source>
        <translation type="vanished">Kann das angegebene Register nicht laden. Nur Register 1 bis 15 sind erlaubt.</translation>
    </message>
    <message>
        <source>Only registers 1 to 15 are allowed.</source>
        <translation type="vanished">Nur Register 1 bis 15 sind erlaubt.</translation>
    </message>
    <message>
        <location filename="virtualmashine.cpp" line="28"/>
        <source>Execution seems to be stuck. Aborting...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="virtualmashine.cpp" line="104"/>
        <source>Can&apos;t load from specified register. Only registers 0 to 15 are allowed.</source>
        <translation>Kann das angegebene Register nicht laden. Nur Register 0 bis 15 sind erlaubt.</translation>
    </message>
    <message>
        <location filename="virtualmashine.cpp" line="116"/>
        <source>Only registers 0 to 15 are allowed.</source>
        <translation>Nur Register 0 bis 15 sind erlaubt.</translation>
    </message>
    <message>
        <location filename="virtualmashine.cpp" line="134"/>
        <source>Cannot devide by zero.</source>
        <translation></translation>
    </message>
</context>
</TS>
