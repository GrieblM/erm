#ifndef MESSAGEBOXSTYLE_H
#define MESSAGEBOXSTYLE_H

#include <QProxyStyle>
#include <QIcon>

class MessageBoxStyle : public QProxyStyle
{
public:
    MessageBoxStyle();

    QIcon standardIcon(StandardPixmap standardIcon, const QStyleOption *option, const QWidget *widget) const override;
};

#endif // MESSAGEBOXSTYLE_H
