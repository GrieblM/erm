#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent = 0);
    ~SettingsDialog();

private slots:
    void on_styleComboBox_currentIndexChanged(int index);

private:
    Ui::SettingsDialog *ui;

    QString currentStyle;
    QFont currentFont;
    int currentFontSize;

public slots:
    void accept() override;
};

#endif // SETTINGSDIALOG_H
