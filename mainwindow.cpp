#if false
#include "settings.h"
#include "settingsdialog.h"
#endif
#include "mainwindow.h"
#include "asmhighlighter.h"
#include "registerdelegate.h"
#include "helpdialog.h"
#include "aboutdialog.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QSvgRenderer>
#include <QPushButton>
#include <QPainter>
#include <QFileDialog>
#include <QTextStream>
#include <QDebug>
#include <QLabel>
#include <QTextBrowser>
#include <QDialog>

#if false
extern Settings settings;
#endif

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    statusBarTimer(new QTimer(this))
{
    ui->setupUi(this);

    highlighter = new AsmHighlighter(ui->editor->document());
    vm = new VirtualMashine(this);
    tk = new Tokenizer(this);
    model = new RegisterItemModel(vm->getRegister(), this);
    statusBarLabel = new QLabel(this);

    ui->registerTable->setItemDelegate(new RegisterDelegate(this));
    ui->registerTable->setModel(model);
    statusBar()->addWidget(statusBarLabel);
    statusBarLabel->setText(tr("Ready"));
    statusBarTimer->setInterval(7500);
    statusBarTimer->setSingleShot(true);

    connect(ui->registersWidget, SIGNAL(visibilityChanged(bool)), ui->actionRegisters, SLOT(setChecked(bool)));
    connect(vm, SIGNAL(stepped(int)), this, SLOT(stepped(int)));
    connect(vm, SIGNAL(error(QString,int)), this, SLOT(runtimeError(QString,int)));
    connect(tk, SIGNAL(error(QString,int)), this, SLOT(runtimeError(QString,int)));
#if false
    connect(&settings, SIGNAL(settingsChanged()), this, SLOT(settingsChanged()));
#endif
    connect(vm->getRunWathcer(), SIGNAL(finished()), this, SLOT(runFinished()));
    connect(vm->getRunWathcer(), SIGNAL(canceled()), this, SLOT(runCanceled()));
    connect(statusBarTimer, SIGNAL(timeout()), this, SLOT(resetStatusBar()));

   if(QApplication::arguments().length() > 1)
   {
       openFile(QApplication::arguments()[1]);
   }
}

MainWindow::~MainWindow()
{
    disconnect(ui->editor, SIGNAL(textChanged()), this, SLOT(onInput()));

    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    QMainWindow::closeEvent(e);
    if(ui->editor->document()->isModified() && !showUnsavedChangesDialog())
        e->ignore();
    else
        e->accept();
}

void MainWindow::on_actionNew_triggered()
{
    //Check if we have a file opened
    if(ui->editor->document()->isModified() && !showUnsavedChangesDialog())
    {
        return;
    }
    ui->editor->clear();
    fileNamed = false;
    fileName = tr("Untitled");
    path = QString();
    fileChanged();
}

void MainWindow::on_actionOpen_triggered()
{
    QString openPath = QFileDialog::getOpenFileName(this, tr("Open assembly file"), "", tr("Simple Assembly file (*.sasm);;Assembly file (*.asm);;All files (*)"));
    if(openPath.isEmpty())
        return;
    openFile(openPath);
}

void MainWindow::on_actionSave_triggered()
{
    if(!fileNamed)
        on_actionSaveAs_triggered();
    else
    {
        QFile file(path);
        if(!file.open(QIODevice::WriteOnly))
        {
            QMessageBox::critical(this, tr("Error"), tr("The file could not be opened.\n%0").arg(file.errorString()));
            return;
        }
        QTextStream stream(&file);
        stream << ui->editor->toPlainText();
        ui->editor->document()->setModified(false);
    }
}

void MainWindow::on_actionSaveAs_triggered()
{
    QString savePath = QFileDialog::getSaveFileName(this, tr("Save file"), "", tr("Simple Assembly file (*.sasm);;Assembly file (*.asm);;All files (*)"));
    if(savePath.isEmpty())
        return;
    QFile file(savePath);
    if(!file.open(QIODevice::WriteOnly))
    {
        QMessageBox::critical(this, tr("Error"), tr("The file could not be opened.\n%0").arg(file.errorString()));
        return;
    }
    QTextStream stream(&file);
    stream << ui->editor->toPlainText();
    fileNamed = true;
    ui->editor->document()->setModified(false);
    path = savePath;
    fileName = path.right(path.length() - path.lastIndexOf(QRegExp("(\\\\|\\/)")) - 1);
    fileChanged();
}

#if false
void MainWindow::on_actionSettings_triggered()
{
    SettingsDialog(this).exec();
}
#endif

void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::on_actionUndo_triggered()
{
    ui->editor->undo();
}

void MainWindow::on_actionRedo_triggered()
{
    ui->editor->redo();
}

void MainWindow::on_actionCut_triggered()
{
    ui->editor->cut();
}

void MainWindow::on_actionCopy_triggered()
{
    ui->editor->copy();
}

void MainWindow::on_actionPaste_triggered()
{
    ui->editor->paste();
}

void MainWindow::on_actionStatusBar_triggered(bool checked)
{
    statusBar()->setVisible(checked);
}

void MainWindow::on_actionRegisters_triggered(bool checked)
{
    ui->registersWidget->setVisible(checked);
}

void MainWindow::on_actionRun_triggered()
{
    if(!loaded)
        load();
    vm->runAsync();
}

void MainWindow::on_actionStep_triggered()
{
    if(!loaded)
        load();
    vm->singleStep();
}

void MainWindow::on_actionReset_triggered()
{
    vm->reset();
}

void MainWindow::on_actionAbout_triggered()
{
    AboutDialog(this).exec();
}

void MainWindow::on_actionHelp_triggered()
{
    HelpDialog(this).exec();
}

void MainWindow::on_stepButton_clicked()
{
    if(!loaded)
        load();
    vm->singleStep();
}

void MainWindow::on_resetButton_clicked()
{
    vm->reset();
}

void MainWindow::runtimeError(QString msg, int line)
{
    statusBarLabel->setText(msg);
    statusBarTimer->start();
    ui->editor->highlightLine(line, true);
}

void MainWindow::onInput()
{
    loaded = false;
    disconnect(ui->editor, SIGNAL(textChanged()), this, SLOT(onInput()));
}

void MainWindow::reset()
{
    loaded = false;
    vm->reset();
}

void MainWindow::stepped(int line)
{
    ui->editor->highlightLine(line);
}

#if false
void MainWindow::settingsChanged()
{
    QApplication::setStyle(settings.getCurrentStyleName());
    ui->editor->setFont(settings.getCurrentFont());
}
#endif

void MainWindow::runFinished()
{
    //QMessageBox::information(this, "Finished", "Execution has finished");
}

void MainWindow::runCanceled()
{
    //QMessageBox::information(this, "Canceled", "Execution has been canceled");
}

void MainWindow::resetStatusBar()
{
    statusBarLabel->setText(tr("Ready"));
}

void MainWindow::load()
{
    bool ok;
    QList<Instruction> instructions = tk->tokenize(ui->editor->toPlainText(), &ok);
    if(ok)
    {
        vm->reinstruct(instructions);
        loaded = true;
        connect(ui->editor, SIGNAL(textChanged()), this, SLOT(onInput()));
    }
}

void MainWindow::fileChanged()
{
    setWindowTitle(tr("%0 - SimpleASM").arg(fileName));
}

/*!
 * Returns \c true if the dialog was not canceled
 */
bool MainWindow::showUnsavedChangesDialog()
{
    QMessageBox dialog(this);
    dialog.setIcon(QMessageBox::Question);

    dialog.setText(tr("Unsaved changes exist in this file. Do you wan't to save it?"));

    dialog.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    dialog.exec();
    QMessageBox::StandardButton btn = dialog.standardButton(dialog.clickedButton());

    switch(btn)
    {
    case QMessageBox::Save:
        on_actionSave_triggered();
    case QMessageBox::Discard:
        return true;
    default:
        return false;
    }
}
bool MainWindow::saveFile()
{
    return 0;
}

void MainWindow::openFile(QString filePath)
{
    QFile file(filePath);
    if(!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::critical(this, tr("Error"), tr("The file could not be opened\n%0").arg(file.errorString()));
        return;
    }
    ui->editor->setPlainText(file.readAll());
    fileNamed = true;
    ui->editor->document()->setModified(false);
    path = filePath;
    //Get the last portion of a string after the path separator (Slash or Backslash)
    fileName = filePath.right(filePath.length() - filePath.lastIndexOf(QRegExp("(\\\\|\\/)")) - 1);
    fileChanged();
}
