#include "registeritemmodel.h"

RegisterItemModel::RegisterItemModel(Register *r, QObject *parent) : QAbstractTableModel(parent)
{
    reg = r;
    connect(reg, SIGNAL(registerChanged(int)), this, SLOT(fireDataChanged(int)), Qt::QueuedConnection);
}

Qt::ItemFlags RegisterItemModel::flags(const QModelIndex &) const
{
    return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
}

int RegisterItemModel::rowCount(const QModelIndex&) const
{
    return 18;
}

int RegisterItemModel::columnCount(const QModelIndex&) const
{
    return 1;
}

QVariant RegisterItemModel::data(const QModelIndex &index, int role) const
{
    if(role == Qt::DisplayRole)
    {
        if(index.row() == 0)
            return QVariant(reg->counter());
        else if(index.row() == 1)
            return QVariant(reg->getAccumulator());
        else
            return QVariant((*reg)[index.row() - 2]);
    }
    return QVariant();
}

QVariant RegisterItemModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole || orientation == Qt::Horizontal)
        return QVariant();
    switch (section)
    {
    case 0:
        return tr("Counter");
    case 1:
        return tr("Accumulator");
    default:
        return tr("Register %0").arg(section - 2);
    }
}

bool RegisterItemModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!(role == Qt::EditRole) || !value.canConvert(QVariant::Int))
        return false;
    if(index.row() == 0)
        reg->setCounter(value.toInt());
    else if(index.row() == 1)
        reg->setAccumulator(value.toInt());
    else
        (*reg)[index.row() - 2] = value.toInt(); //Put the values in the coresponding registers (-2 because row 0 is the counter and 1 the accumulator)
    return true;
}

void RegisterItemModel::fireDataChanged(int index)
{
    dataChanged(createIndex(index, 0), createIndex(index, 0));
}
