#ifndef TOKENIZER_H
#define TOKENIZER_H

#include <QObject>
#include <QList>
#include <QString>
#include <QMap>

#include "vmtypes.h"

class Tokenizer : public QObject
{
    Q_OBJECT
public:
    Tokenizer(QObject *parent = nullptr);

    QList<Instruction> tokenize(QString code, bool* ok);

    QMap<QString, Operator> keywords;
signals:
    void error(QString msg, int line);
};

#endif // TOKENIZER_H
