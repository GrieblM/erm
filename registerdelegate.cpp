#include "registerdelegate.h"
#include <QLineEdit>
#include <QSpinBox>
#include <limits.h>

RegisterDelegate::RegisterDelegate(QObject *parent) : QItemDelegate(parent)
{
}

QWidget *RegisterDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&, const QModelIndex& index) const
{
    QSpinBox *spinBox = new QSpinBox(parent);
    //Our counter shall not go below one, as this represents the first command
    spinBox->setMinimum(index.row() == 0 ? 1 : INT_MIN);
    spinBox->setMaximum(INT_MAX);
    return spinBox;
}

void RegisterDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QSpinBox *spinBox = reinterpret_cast<QSpinBox*>(editor);
    spinBox->setValue(index.data().toInt());
}
